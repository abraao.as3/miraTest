import React from 'react';
import autobind from 'autobind-decorator';
import $ from 'jquery';
import { Grid } from 'semantic-ui-react';

import DB from './DB';
import HomePage from './components/HomePage';
import Page from './components/page/Page';


@autobind
class App extends React.Component{
    @Private _container = null;
    @Private _db = DB.getInstance();

    @Private _home = null;

    state = {
        currentPage: null
    }

    constructor(props){
        super(props);
    }

    componentDidMount(){
        this._container = $('#appContainer');
        this._container.css({
            height: "100vh"
        });

        this._returnHome();
    }

    @Private
    _onchange(value){
        this._db.get(value, this._receiveData);
    }

    @Private
    _receiveData(data){
        if(this._home){
            this._home.close();
        }
        this.setState({
            currentPage: <Page
                data={data}
                returnHome={this._returnHome}
            />
        });
    }

    @Private
    _returnHome(){
        this.setState({currentPage: null});
        this.setState({
            currentPage: <HomePage
                ref={home=>{this._home = home;}}
                change={this._onchange}
            />
        },()=>{
            this._home.start();
        });
    }

    render(){
        return (
            <Grid id="appContainer">                
                {this.state.currentPage}
            </Grid>
        );
    }
}

export default App;
