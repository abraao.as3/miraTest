import 'semantic-ui-css/semantic.min.css';

import React from 'react';
import ReactDom from 'react-dom';
import App from './App';

window.onload = ()=>{
    var rootElement = document.createElement('div');
    document.body.appendChild(rootElement);


    ReactDom.render(<App />, rootElement);
}
