import React from 'react';
import autobind from 'autobind-decorator';
import TweenMax from   'gsap';
import $ from 'jquery';
import { Grid, Card, Dropdown } from 'semantic-ui-react';

@autobind
class HomePage extends React.Component{
    @Private _changeHandler = null;
    @Private _dom = null;

    state = {
        disabled: false
    }

    constructor(props){
        super(props);

        if(this.props.change){
            this._changeHandler = this.props.change;
        }
    }

    componentDidMount(){
        this._dom = $("#homepage");
        TweenMax.to(this._dom, 0, {opacity: 0});
    }

    start(){
        TweenMax.to(this._dom, 1, {opacity: 1});
    }

    close(){
        TweenMax.to(this._dom, 0.5, {opacity: 0});
    }

    @Private
    _onchange(e, data){
        this.setState({disabled: true});
        if(this._changeHandler){
            this._changeHandler(data.value);
        }
    }

    render(){
        return (
            <Grid.Row id="homepage" verticalAlign='middle' centered={true} color="black">
                <Grid.Column mobile={16} tablet={7} computer={7}>
                    <Card centered={true}>
                        <Card.Content>
                            <Card.Header>
                                Star Wars
                            </Card.Header>
                        </Card.Content>
                        <Card.Content>
                            <Dropdown
                                placeholder="Selecione um item"
                                fluid
                                search
                                selection
                                disabled={this.state.disabled}
                                options={[
                                    {text: 'personagens', value: 'people'},
                                    {text: 'filmes', value: 'films'},
                                    {text: 'naves', value: 'starships'},
                                    {text: 'veículos', value: 'vehicles'},
                                    {text: 'raças', value: 'species'},
                                    {text: 'planetas', value: 'planets'}
                                ]}
                                onChange={this._onchange}
                            />
                        </Card.Content>
                    </Card>
                </Grid.Column>
            </Grid.Row>
        );
    }
}

export default HomePage;
