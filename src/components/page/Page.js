import React from 'react';
import autobind from 'autobind-decorator';
import { Grid, Sidebar, Segment, Menu, Card } from 'semantic-ui-react';

import PeopleCard from './PeopleCard';

class Page extends React.Component{
    @Private _data = null;

    state = {
        cards: null
    };

    state = {
        menuvisible: false
    }

    constructor(props){
        super(props);

        this._data = this.props.data;
    }

    componentDidMount(){
        var cards = [];
        var type = this._data.type;

        console.log(type);

        this._data.data.map((val, i)=>{
            var el;
            switch (type) {
                case 'people':
                    el = <PeopleCard
                        key={i}
                        data={val}
                    />
                    break;
            }
            cards.push(el);
        });

        this.setState({cards: cards}, ()=>{
            setTimeout(()=>{
                this.setState({menuvisible: true});
            }, 1000);
        });
    }

    @Private
    _menuClicked(e, data){
        this.props.returnHome();
    }

    render(){
        return (
            <Grid.Row centered={true} color="black">
                <Grid.Column tablet={10} verticalAlign="middle">
                    <Sidebar.Pushable as={Segment}>
                        <Sidebar as={Menu} animation='push' direction='top' visible={this.state.menuvisible} inverted>
                            <Menu.Item onClick={this._menuClicked} name="home">Home</Menu.Item>
                        </Sidebar>

                        <Sidebar.Pusher>
                            <Segment basic>
                                <Grid columns={3}>
                                    {this.state.cards}
                                </Grid>
                            </Segment>
                        </Sidebar.Pusher>
                    </Sidebar.Pushable>
                </Grid.Column>
            </Grid.Row>
        );
    }
}

export default Page;
