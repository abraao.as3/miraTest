import React from 'react';
import autobind from 'autobind-decorator';
import { Grid, Card } from 'semantic-ui-react';

class PeopleCard extends React.Component{
    @Private _data = null;

    constructor(props){
        super(props);

        this._data = this.props.data;
    }

    render(){
        return (
            <Grid.Column textAlign="left">
                <Card>
                    <Card.Content>
                        <Card.Header>{this._data.name}</Card.Header>
                        <Card.Meta>{this._data.birth_year}</Card.Meta>
                    </Card.Content>
                </Card>
            </Grid.Column>
        );
    }
}

export default PeopleCard;
