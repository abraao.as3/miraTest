import axios from 'axios';

class DB {
    @Private _defaultURL = "https://swapi.co/api/";
    @Private _data = {};

    constructor(){
        if(DB._instance){
            throw "use DB.getInstance method."
        }

        DB._instance = this;
    }

    get(query, onloadcallback, page = 1){
        if(!this._data[query]){
            this._data[query] = {};
        }

        if(!this._data[query][page]){
            this._request(this._data[query], query, page, onloadcallback);
            return;
        }

        if(onloadcallback){
            var data = this._data[query][page];
            onloadcallback({total: data.count, pages: data.pages, data: data.results});
        }
    }

    @Private
    _request(stock, query, page, onloadcallback){
        axios.get(this._defaultURL + query + '/?page=' + page)
        .then(res =>{
            var pages = this._countPages(res.data.count, res.data.results.length);

            stock[page] = res.data;
            stock[page].pages = pages;

            if(onloadcallback){
                onloadcallback({type: query, total: res.data.count, pages: pages, data: res.data.results});
            }
        })
        .catch(err =>{
            console.log(err);
        });
    }

    @Private
    _countPages(count, len){
        if(count <= 10) return 1;

        var rest = count % len != 0 ? true : false;
        var pages = Math.floor(count / len);

        if(rest){
            pages = pages + 1;
        }

        return pages;
    }

    static _instance = null;

    static getInstance(){
        if(!DB._instance){
            new DB();
        }

        return DB._instance;
    }
}

export default DB;
