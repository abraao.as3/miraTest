const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/Main.js',
    output: {
        path: path.resolve(__dirname, 'dist/js'),
        filename: 'script.js'
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                include: [path.resolve(__dirname, "src/")],
                loader: "babel-loader",
                options: {
                    presets: [
                        'env', 'react'
                    ],
                    plugins: ['babel-plugin-transform-private-properties', 'transform-decorators-legacy', 'transform-class-properties']
                }
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                loader: 'url-loader',
                options: {
                  limit: 10000
                }
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Mira',
            inject: 'head',
            filename: '../index.html',
            minify: {
                collapseWhitespace: true
            }
        })
    ]
}
